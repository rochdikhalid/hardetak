# Hardetak's Project Plan

## Features

* Create, read, update, and delete tasks.

* Include a tag or label specifying the category of the task.

* Add an option to specify the priority order of the task.

* Provide an option to specify the urgency order of the task.

* Have a checkbox to check off the completed tasks.

* Include a searchable and filterable section that lists completed tasks.

* Authentication system to allow users save their tasks and personalize their experience.

## Basic Tech Stack

* Frontend: HTML/CSS, Bootstrap, Svelte

* Backend: Django

* Database: SQLite (local), and PostgreSQL (prodution)

* Hosting service: Render

## Outline

1. Sketch up an ERD for different tables and fields in the database.
2. Build the database structure using Django.
3. Create a Restful API to allow the frontend to consume the database.
4. Build the UI and set up the SPA using Svelte components.
5. Make API calls to connect the UI with the backend.
6. Test the app and make any necessary adjustments.
7. Share the app with public for feedback and recommendations.
