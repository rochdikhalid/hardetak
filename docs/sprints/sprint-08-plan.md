# Sprint 08 (Mar 13 - Mar 20)

* [x] R0.2-0.2: Change the type of the `is_urgent` field to boolean
* [x] R0.2-0.2: Edit the EDR with the newly added change
* [x] R1-0.2: Design the 404 error page with a descriptive message
* [x] R2-1.5: Review the user interface design for some modification
* [x] R0.1-0.1: Create a new diretory called **client** for `hardetak`'s frontend
* [x] R0.5-0.5: Set up an empty Svelte project within **client** directory
* [x] R0.5-0.5: Create a new web service instance on Render to deploy `hardetak`'s frontend
* [x] R0.5-0.5: Activate the **Build Filters** to trigger deploys for each service