# Sprint 13 (Apr 17 - Apr 24)

* [x] R0.5-0.2: Create an HTML container that contains a title and text input
* [x] R0.2-0.5: Add a filter icon next to the placeholder of the text input
* [x] R0.5-0.2: Style the HTML container according to the agreed design
* [x] R2-1: Implement a collapsible dropdown (Accordion) to show task details

