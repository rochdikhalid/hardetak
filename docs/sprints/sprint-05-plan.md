# Sprint 05 (Feb 20 - Feb 27)

* [x] R2-3: Verify the functionality of the authentication endpoints 
* [x] R1: Conduct research to determine the suitability of testing a third-party authentication library
* [x] R2-1.8: Develop unit tests for the REST API of the `categories` application
* [x] R2-2: Create unit tests for the REST API of the `tasks` application
* [x] R1-1: Configure the CORS and token authentication settings of the authentication system
* [x] R0.2-0.2: Edit the ERD and project plan by adding the newly added changes