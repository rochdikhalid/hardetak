# Sprint 07 (Mar 06 - Mar 13)

* [x] R2-2: Design the home page of `hardetak` and related features
* [x] R1-1: Design the category sections in create, edit, and delete modes
* [x] R2-2: Design the home task page in create, edit, and delete modes
* [x] R1-1: Create a design for tasks filter
* [x] R1-1: Create a design for categories filter
* [x] R1-1: Design the Discard/save changes window
