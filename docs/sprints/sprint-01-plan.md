# Sprint 01 (Jan 23 - Jan 30)

* [x] R2-1.5: Create an ERD to design and represent `hardetak` database
* [x] R0.1-0.1: Create a subfolder within **docs** called **design-assets**
* [x] R0.1-0.1: Save the ERD to **design-assets** subfolder
* [x] R0.1-0.1: Create a folder called **server** for backend code
* [x] R0.1-0.1: Create a Django project named `hardetak` within **server** folder
* [x] R0.2-0.2: Use a free PostgreSQL database instance on Render
* [x] R0.2-0.2: Create a free web service on Render
* [x] R0.5-0.5: Connect the PostgreSQL instance to `hardetak` in production
* [x] R0.5-0.5: Add some basic security practices to the project settings
* [x] R0.2-0.1: Create an `.env` file to store sensitive credentials
* [x] R0.2-0.1: Connect the `.env` file to `hardetak` in production
* [x] R0.1-0.1: Create a new Django application called `users`
* [x] R0.2-0.5: Create a custom User model within the `users` app with admin configuration
* [x] R0.1-0.1: Create a new superuser account in the local server
* [x] R0.2-0.5: Write a script that creates a superuser account during the build 
* [x] R0.1-0.1: Gather staticfiles in one folder and serve them during build
* [x] R0.2-0.2: Use **whitenoise[brotli]** package for better storage
