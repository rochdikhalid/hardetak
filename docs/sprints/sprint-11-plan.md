# Sprint 11 (Apr 03 - Apr 10)

* [x] R1-0.8: Make an API call to register a new user account
* [x] R1-0.4: Add field error validations to the register form
* [x] R0.2-0.1: Redirect to the login page when the user account is created
* [x] R0.5-1: Add flash message to login form for account created notification
* [x] R0.5-0.5: Switch authentication system to token authentication 
* [x] R2-1: Figure out a good layout structure for home and other protected pages
* [x] R0.2-0.2: Reorgnize the project components in a new `lib` folder called **components**
* [x] R2-1: Limit access to home and related pages to authenticated users only
* [x] R2-1.5: Create the header of the authenticated user with their related features
* [x] R1-1: Add the **change password** page with an API call to change the user password
* [x] R0.5-0.1: Add a simple field error validations to the change password form
* [x] R0.5-0.2: Redirect the user to the **login** page after they changed password
* [x] R0.5-0.5: Add flash message to login form for password changed notification
* [x] R0.1-0.1: Add a new component called **Sidebar** to `lib` folder