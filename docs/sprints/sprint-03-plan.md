# Sprint 03 (Feb 06 - Feb 13)

* [x] R0.2-2: Incorporate the Django Rest Framework into `hardetak`
* [x] R1-1: Search for a well documented/tested third-party package for authentication in DRF
* [x] R0.5-1: Use and implement the third-party package endpoints in `hardetak`
* [x] R1-2: Learn and understand how token authentication works compared to session authentication
* [x] R1-2: Configure project settings in `hardetak` to support JWT authentication
* [x] R1-1: Customize the JWT settings for secure authentication
* [x] R1-1: Add another layer of security by blacklisting the old refresh tokens