# Sprint 09 (Mar 20 - Mar 27)

* [x] R1-1: Create a layout component that includes the header and footer of the landing page
* [x] R0.5-0.5: Place the footer at the bottom of the page
* [x] R1-1.5: Change the item in the header based on the current page
* [x] R1-0.1: Add a new CSS file within the `src` folder for global styling
* [x] R2-1: Implement and design the main content of the landing page
* [x] R1-1: Include the SVG paths in a seperate file called `svgPaths.js` within **src** folder
* [x] R1-1: Create Register component and style the **register** page
* [x] R1-0.5: Create Login component and style **login** page
* [x] R1-0.5: CReate Reset password component and style **reset-password** page
* [x] R2-1.5: Make the landing, register, and other pages web responsive