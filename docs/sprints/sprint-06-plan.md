# Sprint 06 (Feb 27 - Mar 06)

* [x] R2-0.1: Test the `dj-rest-auth` package by running the project's unit tests
* [x] R1-0.2: Report the test coverage results to measure the current state of the package
* [x] R2-2.5: Design the landing and authentication pages (login, register, etc)
* [x] R0.5-0.5: Create an initial home page
* [x] R0.5-0.2: Add a new field called **status** to the `Task` model 
* [x] R0.5-0.2: Include the newly added field into the serialized output
* [x] R0.2-0.1: Remove the **is_completed** field from the `Task` model

