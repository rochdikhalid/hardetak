# Sprint 10 (Mar 27 - Apr 03)

* [x] R1-2: Make an API request to authenticate the user with the right credentials
* [x] R0.5-0.5: Redirect the authenticated users to a new test page called **home**
* [x] R2-0.5: Create validation error messages for wrong credentials
* [x] R2-2: Maintain the authentication status when user is authenticated
* [x] R1-1: Make an API request to log out the authenticated user

