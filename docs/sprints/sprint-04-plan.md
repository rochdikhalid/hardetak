# Sprint 04 (Feb 13 - Feb 20)

* [x] R1-2: Understand and reconfigure the JWT authentication settings
* [x] R1-1: Create a REST API for `Category` model 
* [x] R1-0.5: Create a REST API for `Task` model
* [x] R0.5-0.5: Add authentication permission to the REST API of `Category` and `Task` models
* [x] R1-2: Filter the categories based on the current authenticated user
* [x] R0.5-0.8: Restrict users to create a category with any author
* [x] R0.5-0.2: Restrict users to create a task with any author
* [x] R1-0.1: Make the **author** field in `tasks` and `categories` apps hidden