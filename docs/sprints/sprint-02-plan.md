# Sprint 02 (Jan 30 - Feb 06)

* [x] R0.1-0.1: Create a new Django app called **categories**
* [x] R0.5-0.5: Implement the `Category()` model highlighted in the ERD diagram within **categories** 
* [x] R0.5-0.2: Customize the Django admin interface of the `Category()` model
* [x] R0.1-0.1: Create a new Django app called **tasks**
* [x] R0.5-0.8: Implement the `Task()` model highlighted in the EDR diagram within **tasks**
* [x] R0.2-0.1: Customize the Django admin interface of the `Task()` model
* [x] R2-2: Add unit tests to ensure the database models work as expected
* [x] R0.5-0.2: Review the database architecture for improvements
* [x] R0.5-0.5: Gitignore the migrations folder and edit the ERD with the new changes
