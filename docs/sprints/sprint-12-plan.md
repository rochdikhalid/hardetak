# Sprint 12 (Apr 10 - Apr 17)

* [x] R1-1: Customize the `UserDetailsSerializer` class to overide the returned response object
* [x] R1-0.5: Get the username of the authenticated user from the REST API
* [x] R0.5-0.1: Replace the **username** item with the authenticated username value
* [x] R0.2-0.5: Make an API call within **Sidebar** component to get the category names
* [x] R1-1: Use unique `id`  values for each category menu
* [x] R1-2: Style the **Sidebar** component and related elements
* [x] R1-0.1: Create a new component called **Modal**  

