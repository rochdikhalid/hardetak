#!/usr/bin/env bash

# exit an error
set - o errexit

# Create initial superuser
python manage.py shell << EOF

import os
from dotenv import load_dotenv
from users.models import CustomUser


load_dotenv()

CustomUser.objects.create_superuser(
    username=os.getenv('SUPER_USERNAME'),
    email=os.getenv('SUPER_EMAIL'),
    password=os.getenv('SUPER_PASSWORD'))

exit()

EOF
