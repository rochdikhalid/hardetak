#!/usr/bin/env bash

# exit an error
set -o errexit
# install dependencies
pip install -r requirements.txt
# gather and serve static files
python manage.py collectstatic --no-input
# migrate
python manage.py makemigrations
python manage.py migrate
# create an initial superuser account
# ./scripts/create_superuser.sh


