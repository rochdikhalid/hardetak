from django.db import IntegrityError
from django.test import TestCase
from .models import CustomUser


class CustomUserModelTestCase(TestCase):

    def setUp(self):
        self.superuser = CustomUser.objects.create_superuser(
            email='testsuperuser@e.com',
            username='testsuperuser',
            password='H5bE7cK3o'
        )
        self.user = CustomUser.objects.create_user(
            email='testuser@e.com',
            username='testuser',
            password='HRsE3cK7f'
        )

    def test_customuser_string_respresentation(self):
        self.assertEqual(str(self.user), self.user.username)
        self.assertEqual(str(self.superuser), self.superuser.username)

    def test_superuser_account_creation(self):
        self.assertIsInstance(self.superuser, CustomUser)
        self.assertEqual(self.superuser.is_superuser, True)
        self.assertEqual(self.superuser.is_active, True)
        self.assertEqual(self.superuser.is_staff, True)

    def test_user_account_creation(self):
        self.assertIsInstance(self.superuser, CustomUser)
        self.assertEqual(self.user.is_superuser, False)
        self.assertEqual(self.user.is_active, True)
        self.assertEqual(self.user.is_staff, False)

    def test_customuser_email_uniqueness(self):
        with self.assertRaises(IntegrityError):
            CustomUser.objects.create_user(
                email='testuser@e.com',
                username='testuser1',
                password='HSrdYcK7f'
            )

    def test_customuser_username_uniqueness(self):
        with self.assertRaises(IntegrityError):
            CustomUser.objects.create_user(
                email='testuser1@e.com',
                username='testuser',
                password='HSrdYcK7f'
            )
