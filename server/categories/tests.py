from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Category
from .serializers import CategorySerializer
from users.models import CustomUser


class CategoryModelTestCase(TestCase):

    def setUp(self):
        self.author = CustomUser.objects.create_user(
            username='testuser',
            email='testuser@e.com',
            password='H5bE7cK3o'
        )
        self.category = Category.objects.create(
            author=self.author,
            name='test category',
            description='test category description'
        )

    def test_category_string_representation(self):
        self.assertEqual(str(self.category), 'test category')

    def test_category_creation(self):
        self.assertIsInstance(self.category, Category)
        self.assertEqual(self.category.name, 'test category')
        self.assertEqual(self.category.description,
                         'test category description')
        self.assertEqual(self.category.author.username, 'testuser')


class CategoryViewTestCase(APITestCase):

    def setUp(self):
        self.author = CustomUser.objects.create_user(
            username='testuser',
            email='testuser@e.com',
            password='H5bE7cK3o'
        )
        self.client.force_authenticate(user=self.author)
        self.category1 = Category.objects.create(
            name='test category 1', author=self.author)
        self.category2 = Category.objects.create(
            name='test category 2', author=self.author)

    def test_get_queryset_authenticated_user(self):
        url = reverse('category-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = CategorySerializer(
            [self.category1, self.category2], many=True).data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['id'])
        sorted_response_data = sorted(response.data, key=lambda x: x['id'])
        self.assertEqual(sorted_response_data, sorted_serializer_data)

    def test_get_queryset_unauthenticated_user(self):
        self.client.logout()
        url = reverse('category-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_category_create(self):
        url = reverse('category-list')
        data = {'name': 'test category 3'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        category = Category.objects.get(name='test category 3')
        self.assertEqual(category.author, self.author)

    def test_retrieve_category(self):
        url = reverse('category-detail', args=[self.category1.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = CategorySerializer(self.category1).data
        self.assertEqual(response.data, serializer_data)

    def test_update_category(self):
        url = reverse('category-detail', args=[self.category1.pk])
        data = {'name': 'updated test category 1'}
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        category = Category.objects.get(pk=self.category1.pk)
        self.assertEqual(category.name, 'updated test category 1')

    def test_delete_category(self):
        url = reverse('category-detail', args=[self.category1.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Category.objects.filter(
            pk=self.category1.pk).exists())
