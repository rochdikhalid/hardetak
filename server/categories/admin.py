from django.contrib import admin
from .models import Category


class CategoryAdminConfig(admin.ModelAdmin):
    """Customizes fields to add/change Category instances"""

    model = Category
    # fields to be used in displaying the Category table
    list_display = [
        'id',
        'name',
        'description',
    ]
    # fields to be used in changing/updating a category
    fieldsets = (
        (None, {'fields': ('name', 'description')}),
    )
    # fields to be used in creating a new category
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name', 'description'),
        }),
    )

    ordering = ('id',)


admin.site.register(Category, CategoryAdminConfig)
