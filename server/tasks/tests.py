from django.test import TestCase, RequestFactory
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from categories.models import Category
from .models import Task
from .serializers import TaskSerializer
from users.models import CustomUser


class TaskModelTestCase(TestCase):

    def setUp(self):
        self.author = CustomUser.objects.create_user(
            username='testuser',
            email='testuser@e.com',
            password='H5bE7cK3o'
        )
        self.category = Category.objects.create(
            author=self.author,
            name='test category',
            description='test category description'
        )
        self.task = Task.objects.create(
            author=self.author,
            category=self.category,
            title='test task title',
            description='test task description'
        )

    def test_task_string_respresentation(self):
        self.assertEqual(str(self.task), 'test task title')

    def test_task_model_creation(self):
        self.assertIsInstance(self.task, Task)
        self.assertEqual(self.task.title, 'test task title')
        self.assertEqual(self.task.description, 'test task description')
        self.assertEqual(self.task.category.name, 'test category')
        self.assertEqual(self.task.author.username, 'testuser')


class TaskViewTestCase(APITestCase):

    def setUp(self):
        self.author = CustomUser.objects.create_user(
            username='testuser',
            email='testuser@e.com',
            password='H5bE7cK3o'
        )
        self.client.force_authenticate(user=self.author)
        self.factory = RequestFactory()
        self.category = Category.objects.create(
            author=self.author,
            name='test category 1',
            description='test category 1 description'
        )
        self.task1 = Task.objects.create(
            author=self.author,
            category=self.category,
            title='task 1',
            description='task 1 description'
        )
        self.task2 = Task.objects.create(
            author=self.author,
            category=self.category,
            title='task 2',
            description='task 2 description'
        )

    def test_get_queryset_authenticated_user(self):
        url = reverse('task-list')
        # Add user attribute to the request object
        request = self.client.get(url).wsgi_request
        request.user = self.author
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = TaskSerializer(
            [self.task1, self.task2], context={'request': request}, many=True).data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['id'])
        sorted_response_data = sorted(response.data, key=lambda x: x['id'])
        self.assertEqual(sorted_response_data, sorted_serializer_data)

    def test_get_queryset_unauthenticated_user(self):
        self.client.logout()
        url = reverse('task-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_task(self):
        url = reverse('task-list')
        data = {
            'category': self.category.id,
            'title': 'task 3',
            'description': 'task 2 description'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        task = Task.objects.get(title='task 3')
        self.assertEqual(task.author, self.author)

    def test_retrieve_task(self):
        url = reverse('task-detail', args=[self.task1.pk])
        response = self.client.get(url)
        # Add user attribute to the request object
        request = self.client.get(url).wsgi_request
        request.user = self.author
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = TaskSerializer(
            self.task1, context={'request': request}).data
        self.assertEqual(response.data, serializer_data)

    def test_update_task(self):
        url = reverse('task-detail', args=[self.task1.pk])
        data = {
            'category': self.category.id,
            'title': 'task 3 updated',
            'description': 'task 3 description updated'
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        task = Task.objects.get(pk=self.task1.pk)
        self.assertEqual(task.title, 'task 3 updated')

    def test_delete_task(self):
        url = reverse('task-detail', args=[self.task1.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Task.objects.filter(pk=self.task1.pk).exists())
