from rest_framework import serializers
from .models import Task
from categories.models import Category


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = [
            'id',
            'author',
            'category',
            'title',
            'description',
            'priority',
            'is_urgent',
            'status',
            'created_at',
            'updated_at'
        ]
        read_only_fields = ['author']

    def get_fields(self):
        fields = super(TaskSerializer, self).get_fields()
        user = self.context['request'].user
        if user.is_authenticated:
            fields['category'].queryset = Category.objects.filter(author=user)
        return fields

    def create(self, validated_data):
        validated_data['author'] = self.context['request'].user
        return super().create(validated_data)
