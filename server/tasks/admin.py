from django.contrib import admin
from .models import Task


class TaskAdminConfig(admin.ModelAdmin):
    """Customizes fields to add/change Task instances"""

    model = Task
    # fields to be used in displaying the Category table
    list_display = [
        'id',
        'author_id',
        'category',
        'title',
        'description',
        'priority',
        'is_urgent',
        'status'
    ]
    # fields to be used in changing/updating a category
    fieldsets = (
        (None, {'fields': ('author', 'category', 'title', 'description',
                           'priority', 'is_urgent', 'status')}),
    )
    # fields to be used in creating a new category
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('author', 'category', 'title', 'description', 'priority', 'is_urgent', 'status'),
        }),
    )

    ordering = ('id',)


admin.site.register(Task, TaskAdminConfig)
