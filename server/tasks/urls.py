from django.urls import include, path
from rest_framework import routers
from .views import TaskViewSet


router = routers.DefaultRouter()
router.register(r'', TaskViewSet, basename='task')


urlpatterns = [
    path('', include(router.urls)),
]
