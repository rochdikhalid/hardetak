from django.db import models
from categories.models import Category
from users.models import CustomUser


class Task(models.Model):
    """Structures the Task table in the database"""

    PRIORITY_CHOICES = [
        ('H', 'High'),
        ('M', 'Medium'),
        ('L', 'Low'),
    ]
    STATUS_CHOICES = [
        ('P', 'Pending'),
        ('O', 'Ongoing'),
        ('D', 'Done'),
    ]

    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=75)
    description = models.CharField(max_length=150)
    priority = models.CharField(
        max_length=1, choices=PRIORITY_CHOICES, blank=True, null=True)
    is_urgent = models.BooleanField(default=False)
    status = models.CharField(
        max_length=1, choices=STATUS_CHOICES, default='P')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created_at']
        verbose_name_plural = 'tasks'
