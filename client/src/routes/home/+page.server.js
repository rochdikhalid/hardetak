import { redirect } from '@sveltejs/kit';

import { isAuthenticated } from '$lib/stores';


export const load = async ({ request }) => {
	const isUserAuthenticated = await isAuthenticated;
	if (!isUserAuthenticated) {
    	throw redirect(302, '/login');
	}
};