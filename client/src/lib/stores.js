import { writable } from 'svelte/store';


export const flash = writable(null);
export const isAuthenticated = writable(false);